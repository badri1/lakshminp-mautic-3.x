FROM lakshminp/php:7.2.24-fpm-buster

COPY mautic-2.x /var/www/symfony

RUN useradd -u 1001 -r -g 0 -d /app -s /bin/bash -c "Default Application User" default \
    && chown -R 1001:0 /var/www/symfony && chmod -R g+rwX /var/www/symfony

WORKDIR /var/www/symfony

RUN mkdir -p /var/www/symfony/translations

RUN composer install --no-dev --prefer-dist --no-interaction --no-ansi --optimize-autoloader

RUN chown -R 1001:0 /var/www/symfony/app/cache && chmod -R g+rwX /var/www/symfony/app/cache
RUN chown -R 1001:0 /var/www/symfony/app/logs && chmod -R g+rwX /var/www/symfony/app/logs

USER 1001
